---
title: Version v4.1.2
permalink: /blog/version-4-1-2/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ new columns for HDR and aspect ratio - thx @icebaer72  
\+ (movies/TV shows) re-added persisting of last used filters #1153  
\+ (movies) option to only include the first studio in the NFO  
\+ (movies) added cover.ext to well known poster file names  
x removed unused code fot TV show ratings #1233  
x display custom rating source logo #1233  
x do not delete tags on further scrapes #1233  
x (movies/TV shows) saving changed data sources in settings  
x (movies/TV shows) do not write NFO on update data sources  
x (TV shows) do not strip out TV show title from inside the episode title  
x changed renamer token ${hdr} to ${hdrformat}  
x better propagation of API errors to the UI #1237  
x (TV shows) reworked scrape new items  
x (macOS) search for Kodi scrapers in different locations  
x (TV shows) writing of correct NFO file name for BD/DVD episodes #1248  
x (movies/TV shows) fixed update data sources for git-annex setups #1223  
x run some long running actions in a background task  
x (movies) sorting of MS/RT columns  
x (movies/TV shows) fixed rendering issues of bidirectional text  