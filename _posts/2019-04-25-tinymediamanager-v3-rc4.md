---
title: version v3.0 RC4
permalink: /blog/version-3-0-RC4/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---

tinyMediaManager v3 - RC4 is out containing many fixes/improvements over RC3:

\+ added support for characterart for TV shows  
\+ added support for keyart for movies and TV shows  
\+ added a "missing episode list" (thx @wjanes)  
\+ added support for image check (now you can configure which artwork must be available for the green checkmark - thx @wjanes)  
\+ added video bit depth to the renamer  
\+ added colon replacement with spaces  
\+ added some more media info logos  
\+ completely refactored Trakt.tv integration  
\+ save trace logs from the last session (for better/easier bug hunting)  
\+ major rework of the media files panel  
\+ enhanced the wizard by UI settings  
\+ added support for folder.ext for season poster  
x fixed some movie set artwork hiccups  
x use the new date formatter in the movie table too   
x make settings dialog scrollable  
x IMDb id can now be bigger  
x added .mk3d to supported file extensions    
x respect movie trailer settings on download  
x fixed some libmediainfo detections  
x many fixes in the TV show tree (performance and stability)  
x only write guests to episode NFO files  
x do not use libmediainfo for getting the "date added"  
x Kodi RPC - use asynchronous connection (do not block startup if Kodi is not available)  
x better sorting of media files (by type and size now)  
x updated glazedlists to 1.11.0  
x do not offer checkboxes for extrathumbs/extrafanart when not activated in settings  
x added more translateable texts for scraper settings  
x many code cleanups  


Please report any issues/feature requests at [our project home](https://gitlab.com/tinyMediaManager/tinyMediaManager/).
