---
title: JMTE replacement renderer
permalink: /blog/jmte-replacement-renderer/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

tinyMediaManager v4.1 introduced a new JMTE renderer (`replace`) to do custom replacements. Those replacements can be defined by the user by placing a CSV file containing all desired replacements in the tinyMediaManager `data` folder. The CSV file needs to the separated by a comma (`,`) and the values _can_ be escaped with double quotes (`"`). 

And example csv file could look like (the example also covers some of the options which are hardcoded in tinyMediaManager):

```
Ä,A
ä,a
Ö,O
ö,o
Ü,U
ü,u
ß,ss
:," "
" ",.
```

Those replacements will be processed in the same order as in the CSV file (top to bottom, so e.g. the colon will be replaced by a space and afterwards the space will be replaced by a dot).

The contents of the CSV file will be cached for 60 seconds to avoid excessive reading of the file
{: .notice--primary}

The renderer can be used like `${movie.title;replace(umlauts.csv)}` to load the replacement tokens from the file `/data/umlauts.csv` and apply it to the movie title. As you can see here, you are able to define different replacement files and you are able to specify at every token which preset file you want to use.

This can also be used to _override_ some tinyMediaManager internal values like Blu-ray:

```
"Blu-ray","BluRay"
```

and use `${mediaSource;replace(mediasource.csv)}`