---
title: Version v4.1.1
permalink: /blog/version-4-1-1/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ filter for note field  
x (TV show) fixes endless update data source #1230 #1228  
x (TV show) do not select all episodes in TV show rename action per default  
x better visualization of failed download #1229  
x increased max memory limit to 8G  
x fix changing maximum download thread count at runtime #1232  
x sort showlinks in the dropdown box #1231  