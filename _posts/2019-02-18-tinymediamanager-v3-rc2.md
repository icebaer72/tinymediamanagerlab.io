---
title: Version v3.0 RC2
permalink: /blog/version-3-0-RC2/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---
RC2 of tinyMediaManager v3 is out, containing the following changes:

\+ added support for searching by IMDB id  
\+ moved the whole infrastructure over to GitLab (source code hosting, building and binary hosting)  
\+ updated to libmediainfo 18.12 and adopted to the new libmediainfo format  
\+ write the added-date to NFOs too  
\+ added original title to TV shows too  
\+ create a backup of the whole /data folder (including settings) instead of only the databases  
\+ added filter for container format (movies and TV shows)
\+ added certification, edition and source columns to the movie table  
\+ added update movie to the popup menu  
\+ added capitalize words for movie and TV show renamer (thx @wjanes)  
\+ added a function to detect unwanted files in your data sources (thx @wjanes)  
\+ added some bulk editor additions (thx @wjanes)  
\+ added other IDs to the details panel too  
\+ added webm to supported video formats per default  
\+ added trailer-name settings  
x always move files/folders to the .deletedByTmm folder instead of deleting them  
x do not create season folders when changing the renamer patterns  
x treat all images in /extrafanarts as extrafanarts  
x enhanced drawing of tooltips in the tree/table  
x enhanced startup to detect custom JVMs  
x some fixes to the multiple ratings infrastructure  
x force popups to be shown in the front  
x fixed some genre names  
x some fixes/enhancements to the universal scraper  
x better detection of the screen size (avoid tmm to be too large on multi screen setups)  
x reduced minimal window width of tmm to 1050px  
x re-added DataTablesHTML export template  
x performance enhancements in the duplicate filter (movies)  
x fixed path comparison in the renamer (case sensitive checks on case insensitive file systems)  
x enhanced media info 3D detection: differentiate between half/full resolutions    
x fixed last modification time detection on macOS  
x fixed some Java 9 - 11 issues  
x separated cast, studio and country at scraping (thx @wjanes)  
x many new/fixed media info icons  
x fixed an exception in the column configurator for renamer settings  
x fixed some issues for Turkish users  
x fixed redrawing of changed artwork  
x improved trailer detection  
x removed multi selection from data source maintenance screen  
x enhanced renamer patterns of audio streams  
