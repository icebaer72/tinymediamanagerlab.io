---
title: Aspect Ratio Detection
permalink: /blog/aspect-ratio-detection/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

tinyMediaManager's newest release (4.1.5) added automatic detection of the "real" aspect ratio of movies and TV shows. Many video files are encoded in 16:9 (1.78:1) aspect ratio even if the production was shot in a wider format (e.g. widescreen 2.40:1). In this case the video file includes black bars above and below the actual image. So far, the aspect ratio and video resolution of the encoded video were included in media information data instead of the real values.

Now tinyMediaManager detects the real image area and includes the aspect ratio and the size of the cropped image (excluding black bars) in the media information. This new feature is included in both the **FREE** and **PRO** versions.

## Getting started with aspect ratio detection

To use aspect ratio detection, first configure FFmpeg (Settings -> General -> System). Aspect ratio detection can then be started in the edit module (Media files -> Detect aspect ratio) or in the context menu of the movie panel (Enhanced editing -> Detect aspect ratio).

To speed up detection, several short samples of the video are analyzed (instead of scanning the complete video file). The samples are distributed over the video duration. The number of samples that are analyzed can be configured to balance between speed and accuracy of the aspect ratio detection (Settings -> General -> Aspect Ratio Detector). If only few samples are analyzed and these samples contain dark scenes the detection might be less accurate. Possible settings are _fast_, _default_ and _accurate_.

The calculated aspect ratio is then rounded to one of several user-selectable industry standard aspect ratios.

## Multi format movies

Some movies have more than one aspect ratio: e.g. some scenes were filmed in 16:9 (1.78:1) format while others were shot in 70mm (2.20:1). tinyMediaManager can also detect multi format movies if _accurate_ detection is selected.

In the settings you configure how multi format movies will be detected:
  * **No** - Use the aspect ratio that is most frequently found. 
  * **Yes, use higher aspect ratio (e.g. for TV or 16:9 projection)** - Multi format movies are detected. The higher (narrower) aspect ratio (with the smaller aspect ratio value) is used. This is useful for TVs and projection on 16:9 screens: A scene with a "high" aspect ratio should fill the complete height of the screen. You will see format changes during the movie. Black bars will appear at the top and bottom in "wide" scenes. The complete image content is shown, and nothing is cropped.
  * **Yes, use wider aspect ratio (e.g. for 21:9 projection)** - Multi format movies are detected. The wider aspect ratio (with the bigger aspect ratio value) is used. This is useful if you project on a 21:9 screen and you do not like to see any black bars: The image should be zoomed to fill the complete width of the screen. To avoid projecting "high" aspect ratio scenes outside of your screen the areas above and below the screen should be masked in the projector. Then you will watch a wide screen movie with a constant aspect ratio and without any format changes. But you will also miss some image content at the top and bottom in "high" aspect ratio scenes since this is cropped. This is how many movie theaters present multi format movies.

## What can you do with aspect ratio information?

You could use the "real" aspect ratio data to add more accurate information to your media library or just create some statistics. Home theater enthusiasts could add the movie's real aspect ratio to the filename using tinyMediaManager's movie & TV show renamer. For example the renamer tag `${_AR,aspectRatio,}` would append the aspect ratio to the filename (e.g. _AR178 for 1.78:1 or _AR240 for 2.40:1). A home theater automation could parse the filename, extract the information, and set the lens zoom of the projector according to the aspect ratio of the movie that is currently playing. Or an automatic masking curtain in your home theater could move to the correct position shortly before the video begins.