---
title: Migrate data sources to a new location
permalink: /blog/migrate-datasource/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

Starting with tinyMediaManager v4.1 you will have the possibility to _exchange_ a data source with another path in all your movies/TV shows. This might be handy if you change mount points on your system, or copy the whole tinyMediaManager installation to another system. With this feature you don't need to rebuild the whole database after changing the path of the data source.

To migrate a data source to the new location, just go to the data source settings page and press button with the two arrows (on the bottom of the list) to exchange the path of the selected data source with a new path (and also in all movies/TV shows of this data source)

<a class="fancybox" href="{{ site.urlimg }}2021/01/migrate_datasource.png" rel="post" title="Migrate data source">![Migrate data source]({{ site.urlimg }}2021/01/migrate_datasource.png "Migrate data source"){: .align-center}</a>
