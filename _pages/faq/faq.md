---
layout: single
permalink: "/help/faq"
title: "Frequently Asked Questions"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "help"
---

## General

### Is tinyMediaManager free to use?
You can [download tinyMediaManager](/download) and use it for free in a limited way (FREE version). If you want to unlock all features you need to buy a license for the PRO version. They key differences are:

- FREE version:
  + base functionality of tinyMediaManager (update data sources, scrape, write/read NFO files, rename, edit, export, command line interface, ...)
  + TMDB scraper

- PRO version:
  + all other scrapers (IMDB, OMDB, Universal, Kodi, ...)
  + trailer download
  + subtitle download
  + Trakt.tv integration
  + FFmpeg integration

If you want to unlock the PRO version of tinyMediaManager, you can [buy a license](/purchase/).

### Does tinyMediaManager modify files from my library?
tinyMediaManager will not alter files from your media library unless you force it to by using:

* scrape meta data (NFO files and artwork files will be created/overwritten)
* rename (the media files including all necessary files like artwork and NFOs will be renamed according to your settings)
* delete (the media files (including all meta data files) of the selected movie(s)/TV show(s)/episode(s) will be deleted from the file system)

### I needed to reinstall tinyMediaManager - can I import old data?
Since tinyMediaManager is designed in a portable manner, all data is stored inside the tinyMediaManager folder itself. To use your data from the old installation, just copy the folders `data` (configuration files and databases) and `cache` from the old tinyMediaManager installation to the new one. Make sure your media is accessible via the same paths (mounts) or the entries from the database point to the wrong destination.

### I messed up my database/settings - how can I undo those changes?
tinyMediaManager creates a backup every day in the `backup` folder of the tinyMediaManager installation. As long as you did not rename or remove the files on your storage, you can simply roll back the database/settings to an old state by extracting the contents of the desired backup file into the `data` folder of tinyMediaManager.

### Re-import NFO changes in Kodi
Once a movie or TV show has been imported into Kodi, Kodi does not react on (external) changes to the NFO file. You can force Kodi to refresh the data by following the steps on the [Kodi wiki](http://kodi.wiki/view/Set_content_and_scan#Refreshing_the_library).

### How to handle bonus content
Bonus content like cut scenes or some extra clips may cause some import problems when not having the right name scheme (tinyMediaManager detects them as additional movies).

The best way to put bonus content to your movie is to create a subfolder called `extras` inside the movie folder and put all bonus content inside this folder.

Another way would be to add the string `-extras` (or `-behindthescenes`, `-deleted`, `-featurette`, `-interview`, `-scene`, `-short`) at the end of every bonus content file like `Aladdin Cut Scenes-extras.avi`. This way tinyMediaManager knows that these files are extra files for your movie and assigns them to the corresponding movie rather than creating a new movie.

### Import watched state into Kodi
By default Kodi does not import the watched state from the NFO into its internal database. You can force Kodi to import the state by following the steps from the [Kodi wiki](http://kodi.wiki/view/Import-export_library#Watched_status).

## License

### Where do I enter the license code?
You can enter the license code either directly in tinyMediaManager (**Unlock** button on the right side of the toolbar or via the menu **Info -> Register tinyMediaManager**). If you use the command line version, just paste the license code without any extra spaces/newlines into a file called `data/tmm.lic` inside your tinyMediaManager folder.

### Can I use the license on more than one tinyMediaManager instances?
The license is per user - you can use it on as many tinyMediaManager installations as you wish.

### Where is the license information stored?
tinyMediaManager stores the license code in a file called `data/tmm.lic` inside the tinyMediaManager folder.

### I did not receive any license
If you did not received an email from [Paddle](https://www.paddle.com) please check your *Spam / Junk Email folder* (if you use a desktop application, please check that folders on the web interface too). If you have not received any email at all then you have likely mistyped your email address or your mail server is rejecting emails. In this case, please contact [contact Paddle](https://paddle.net/contact) with *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.

### I have lost my license code
If you have lost your license code, please [contact Paddle](https://paddle.net/contact) with your *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.

## Errors and bugs

### File corrupted error
If tinyMediaManager has been shut down in an unsafe way (e.g. killing the process via task manager, hard crash of the JVM, ...) while writing to to internal database, your database can get corrupted. You will get the error `java.lang.IllegalStateException: File corrupted` on the next startup of tinyMediaManager. Unfortunately there is no way to repair the database, but tinyMediaManager creates backup files which can be used to replace the damaged database file(s):

* Go to the `backup` folder inside your tinyMediaManager installation (macOS users: right click the tinyMediaManager.app -> Show package contents -> navigate to Contents - Resources - Java)
* extract the database(s) (`movies.db` and/or `tvshows.db`) of the desired backup file into the `data` folder of tinyMediaManager
  * if the latest backup does not work, try the backup from the previous day

### Java Heap Space errors
If you got a large library, the preserved memory (512MB default) for the tinyMediaManager process can be reached. You are able to increase the memory by a dedicated setting (Settings -> General -> System) or by editing a config file directly:

1. open (or create if it does not exist) the file `launcher-extra.yml` in the tinyMediaManager folder
2. add (or modify) the following part in this file:
```
jvmOpts:
- "-Xmx1024m"
```
(where 1024 is the amount of megabytes you want to preserve for tinyMediaManager; you can change it as you want. **Be aware**: if the value is too high for your system, tinyMediaManager may reject to start at all

**v3**: You can create (or edit if it already exists) a file called `extra.txt` in the tinyMediaManager install directory and add (or modify) the following line `-Xmx1024m` (where 1024 is the amount of megabytes you want to preserve for tmm; you can change it as you want. **Be aware**: if the value is too high for your system, tinyMediaManager may reject to start at all
{: .notice--warning}

Java needs this amount of memory to be available on startup of tinyMediaManager. If there is not enough memory available on your system, Java prevents tinyMediaManager from starting _without_ any message. If you encounter this problem, just try to lower the memory setting.

### The upgrade to v4 did not work
Sometimes (depending on your setup) the upgrade from v3 to v4 does not work, but there is a [manual way to upgrade the installation](/docs/upgrade-v4).

You can also go to v3 again, if v4 does not work for you - just make a clean v3 install, and extract the contents of the file `data/v3-backup.zip` into the new v3 install folder.

### Scraping does sometimes not work
Sometimes we receive garbled/unparseable responses from the meta data providers (or some devices along the connection to the servers like gateways). Since we are using some sort of HTTP caching, these unparseable responses are cached for a while with a result, that a further scraping will also result in an error.

If you encounter that problem you can try to clear the cache via the menu

* Tools -> Clear HTTP cache

### tinyMediaManager won't start
We've built tinyMediaManager as fail safe as possible. But there are always combinations of Java/OS/configurations possible which could prevent it from starting. Have a look at the OS specific sections or at the *Java Heap Space* sections if there are any hints for getting tinyMediaManager to start.

If the hints did not help, please collect the following logs which are produced by two steps of starting tinyMediaManager:

* The updater, which fetches the latest version of tinyMediaManager directly from the web (`launcher.log`)
* Launching of tinyMediaManager itself (`logs/tmm.log` and/or `logs/trace.log`)

In the case tinyMediaManager won't bring up his own UI, please create a bug report at [GitLab](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues) and attach the log files there.
