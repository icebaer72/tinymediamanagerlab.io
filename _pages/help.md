---
layout: single
classes: wide
permalink: "/help/"
title: "Help"

sidebar:
  nav: "help"
---

## You can find help at

* our [documentation](/docs/)
* the [Frequently Asked Questions](/help/faq)
* the tinyMediaManager section on [Reddit](https://www.reddit.com/r/tinyMediaManager/)


## Bugs/Feature requests

You can submit a [Bug Report](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues/new?issuable_template=Bug) or [Feature Request](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues/new?issuable_template=Feature%20Request).

## Contact

If you have problems with your license/payment you can also contact us by mail via **support@tinymediamanager.org**. Please try to find help on [Reddit](https://www.reddit.com/r/tinyMediaManager/) if you have general questions.
