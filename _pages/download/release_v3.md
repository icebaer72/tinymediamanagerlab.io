---
title: "Download"
layout: single
classes: wide

permalink: /download/release-v3/

sidebar:
  nav: "downloads"
---

You need at least Java 1.8 to run tinyMediaManager v3. You can get Java from [https://www.java.com/](https://www.java.com/) (Linux users can download Java from the package repository of their distribution too).

<iframe src="https://release.tinymediamanager.org/download_v3.html" style="width:100%; height:400px;border:none;">
<p>You find the latest release at <a href="https://release.tinymediamanager.org">https://release.tinymediamanager.org</a></p>
</iframe>

<br />
All downloads are available at [https://release.tinymediamanager.org](https://release.tinymediamanager.org).