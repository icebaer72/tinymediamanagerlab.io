---
layout: single
permalink: "/docs/commandline"
title: "Command Line Arguments"

sidebar:
  nav: "docs"
---
tinyMediaManager offers a command line interface (CLI) to execute common tasks without the user interface (e.g. on headless servers).

## tinyMediaManager v4
To start tinyMediaManager via the command line you can use the dedicated executable for command line arguments:

* Windows: **tinyMediaManager.exe**
* Linux: **tinyMediaManager**
* macOS: **tinyMediaManager** (note: the executable is inside the tinyMediaManager.app, in the subfolder `Contents/macOS/`)

Execute with the `-h` parameter to display the syntax. There are three parameters for the main command `tinyMediaManager`:

| Parameter | |
| --------- | --- |
| `-h` | display the help and exit |
| `--update` | update tinyMediaManager from the command line)
| `-V` | display the version and exit |

### Movie module
the **movie** subcommand offers various tasks for the movie module (`tinyMediaManager movie`):

| Parameter | |
| --------- | --- |
| `-h` | display the help and exit |
| `-u, --updateAll` | Scan all data sources for new content |
| `--updateX=<index>` | Scan the given data sources for new content. The indices are in the same order as in the UI/settings |
| `-n, --scrapeNew` | Scrape new movies (found with the update options) |
| `--scrapeUnscraped` | Scrape all unscraped movies |
| `--scrapeAll` | Scrape all movies |
| `-t, --downloadTrailer` | Download missing trailers |
| `-s, --downloadSubtitles` | Download missing subtitles |
| `-sL, --subtitleLanguage=<language>` | Desired subtitle language(s) (optional) |
| `-r, --renameNew` | Rename & cleanup all movies from former scrape command |
| `--renameAll` | Rename & cleanup all movies |
| `-e, --export` | Export your movie list using a specified template |
| `-eT, --exportTemplate=<template>` | The export template to use. Use the folder name from the templates folder |
| `-eP, --exportPath=<path>` | The path to export your movie list to |
| `-w, --rewriteNFO` | Rewrite NFO files of all movies |
| `-d, --ardNew`  | Detect aspect ratio of new movies (found with the update options) |
| `-dA, --ardAll` | Detect aspect ratio of all movies |
| `-V` | display the version and exit|

**Examples**

`tinyMediaManager movie -u -n -r` to find/scrape and rename new movies

`tinyMediaManager movie -t -s` to download missing trailer/subtitles

`tinyMediaManager movie -e -eT=ExcelXml -eP=/user/export/movies` to export the movie list with the ExcelXml template to /user/export/movies

### TV show module

the **tvshow** subcommand offers various tasks for the TV show module (`tinyMediaManager tvshow`):

| Parameter | |
| --------- | --- |
| `-h` | display the help and exit |
| `-u, --updateAll` | Scan all data sources for new content |
| `--updateX=<index>` | Scan the given data sources for new content. The indices are in the same order as in the UI/settings |
| `-n, --scrapeNew` | Scrape new TV shows/episodes (found with the update options) |
| `--scrapeUnscraped` | Scrape all unscraped TV shows/episodes |
| `--scrapeAll` | Scrape all TV shows/episodes |
| `-t, --downloadTrailer` | Download missing trailers |
| `-s, --downloadSubtitles` | Download missing subtitles |
| `-sL, --subtitleLanguage=<language>` | Desired subtitle language(s) (optional) |
| `-r, --renameNew` | Rename & cleanup all TV shows/episodes from former scrape command |
| `--renameAll` | Rename & cleanup all TV shows/episodes |
| `-e, --export` | Export your TV show list using a specified template |
| `-eT, --exportTemplate=<template>` | The export template to use. Use the folder name from the templates folder |
| `-eP, --exportPath=<path>` | The path to export your TV show list to |
| `-w, --rewriteNFO` | Rewrite NFO files of all TV shows/episodes |
| `-d, --ardNew`  | Detect aspect ratio of new TV shows/episodes (found with the update options) |
| `-dA, --ardAll` | Detect aspect ratio of all TV shows/episodes |
| `-V` | display the version and exit |

**Examples**

`tinyMediaManager tvshow -u -n -r` to find/scrape and rename new TV shows/episodes

`tinyMediaManager tvshow -t -s` to download missing trailer/subtitles

`tinyMediaManager tvshow -e -eT=TvShowDetailExampleXml -eP=/user/export/tv` to export the TV show list with the TvShowDetailExampleXml template to /user/export/tv

### tinyMediaManager v3
To start tinyMediaManager via the command line you can use the dedicated executable/shellscript for command line arguments:

* Windows: **tinyMediaManagerCMD.exe**
* Linux: **tinyMediaManagerCMD.sh**
* macOS: **tinyMediaManagerCMD-OSX.sh** (note: the shellscript is inside the tinyMediaManager.app, in the subfolder `Contents/Resources/Java`)

Execute without any parameter to display the syntax.

| Parameter | |
| --------- | --- |
| `-updateMovies` | update all movie datasources and add new movies/files to the database |
| `-updateMoviesX` | replace X with 1-9 - just updates a single movie datasource; ordering like GUI |
| `-updateTv` | update all TvShow datasources and add new TvShows/episodes to the database |
| `-updateTvX`| replace X with 1-9 - just updates a single TvShow datasource; ordering like GUI |
| `-update` | update all (short for ‘-updateMovies -updateTv’) |
| `-scrapeNew` | auto-scrape (force best match) new found movies/TvShows/episodes from former update(s) |
| `-scrapeUnscraped` | scrape all unscraped movies (independent from update) |
| `-scrapeAll` | ALL movies/TvShows/episodes, whether they have already been scraped or not |
| `-rename` | rename & cleanup all the movies/TvShows/episodes from former scrape command |
| `-config \<file.xml\>` | specify an alternative configuration xml file in the data folder |
| `-export \<template\> \<dir\>` | exports your complete movie/tv library with specified template to dir |
| `-checkFiles` | does a physical check, if all files in DB are existent on filesystem (might take long!) |

so a ```tinyMediaManagerCMD.exe -update -scrapeNew -renameNew``` perfectly cleans your new and updated items :)

NOTE: if you start tinyMediaManager this way, you won’t get an UI or updates!
You can update tinyMediaManager either by starting the UI or starting the updater executable for your system:

* Windows: **tinyMediaManagerCMDUpd.exe**
* Linux: **tinyMediaManagerUpdaterCMD.sh**
* macOS: **tinyMediaManagerUpdaterCMD-OSX.sh** (note: the shellscript is inside the tinyMediaManager.app, in the subfolder `Contents/Resources/Java`)
