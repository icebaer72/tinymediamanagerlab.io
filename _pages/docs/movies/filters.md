---
layout: single
permalink: "/docs/movies/filters"
title: "Filter and Sort Options"

toc: false

sidebar:
  nav: "docs"

gallery:
  - url: images/docs/movies/movies17.png
    image_path: images/docs/movies/movies17.png
  - url: images/docs/movies/movies18.png
    image_path: images/docs/movies/movies18.png

---

There are many filters available in the movie section which help you to find/filter the movies you are searching for. These filters are applied either to meta data or to the media information of your movies.

{% include gallery id="gallery" %}

You can filter the movie list by the following items:

* New movies
* Show duplicates: a duplicate search on IMDB Id and TMDB Id is done and all duplicates are shown
* Watched flag: you can either display all watched or all unwatched movies
* Genre: show all movies which have at least the selected genre
* Certification
* Year
* Crew/Cast member: show all movies containing an actor which name matches to the inserted text
* Country
* Language
* Production company
* Tag: show all movies having at least the given tag(s)
* Edition
* Movies in movieset
* Resolution
* Video codec
* Aspect ratio
* Frame rate
* 3D
* Container format
* Audio codec
* Data source
* Media source
* With extras
* Missing meta data: show all movies with missing meta data
* Missing artwork: show all movies with missing artwork
* Missing subtitles: show all movies without subtitles


In addition to the filters your are also able to sort your movies by several fields:

* Title
* Sort title
* Year
* Date added
* Release date
* Watched flag
* Rating
* Runtime
* Video bitrate
* Frame rate
