---
layout: single
permalink: "/docs/tvshows/renamer"
title: "TV Show Renamer"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

tinyMediaManager offers a powerful renamer to rename your TV shows and all associated files to your desired folder-/filenames. While there is almost nothing you can't do with the renamer, it has still one big restriction: you can only rename the TV shows inside its own data source. Renaming it to a destination which is not inside the own data source is not supported.

## Renamer Pattern ##

**Folder name**, **Season folder name** and **Episode filename**: Choose the desired folder name(s) and filename for renaming. Here you can enter fixed parts of the name and dynamic ones. You will find a list of all available tokens for building up dynamic names beneath the settings along with examples of your media library. With leaving the folder name/filename empty, the renamer will skip the name generation for the empty part.

The renamer is built on [JMTE](/docs/jmte) and can profit from all options you can have in JMTE. For easier usage, we have added some preset tokens (see table below). Nevertheless you can also access every field from the movie just by using the full syntax like `${tvShow.title}` or `${episode.title}`. **Note**: if the preset token already contains a renderer (like `${filesize}` -> `${moepisode.videoFilesize;filesize}`), you cannot add another renderer. In this case you need to switch to the full syntax.

### Available tokens ###

| Token | Description | Example |
|-------|-------------|---------|
|${title}<br>${episode.title}|Episode title |	Pilot|
|${originalTitle}<br>${episode.originalTitle} |	Episode original title|Pilot|
|${titleSortable}<br>${episode.titleSortable} |	Episode title sorting (e.g. Luminous Fish Effect, The)|	Pilot|
|${seasonNr}<br>${episode.season;number(%d)} | Season number	|1|
|${seasonNr2}<br>${episode.season;number(%02d)}	| Season number with 2 digits	|01|
|${seasonNrDvd}<br>${episode.dvdSeason;number(%d)}	| DVD season number	|1|
|${seasonNrDvd2}<br>${episode.dvdSeason;number(%02d)}	| 2 digit DVD season number|	01|
|${episodeNr}<br>${episode.episode}	| Episode number	|1|
|${episodeNr2}<br>${episode.episode;number(%02d)}	| Episode number with 2 digits|	01|
|${episodeNrDvd}<br>${episode.dvdEpisode}	| DVD episode number	|1|
|${episodeNrDvd2}<br>${episode.dvdEpisode;number(%02d)} | 2 digit DVD episode number|	01|
|${airedDate}<br>${episode.firstAired;date(yyyy-MM-dd)} | Episode aired date (yyyy-MM-dd)	|2008-01-20|
|${year}<br>${episode.year} |	Episode Year	|2008|
|${seasonName}<br>${season.title} |	Season Name	|My personal note|
|${showTitle}<br>${tvShow.title} |	TV show title	|Breaking Bad|
|${showYear}<br>${tvShow.year} |	TV show Year	|2008|
|${showOriginalTitle}<br>${tvShow.originalTitle} |	TV show original title|	Breaking Bad|
|${showTitleSortable}<br>${tvShow.titleSortable} |	TV show title sorting (e.g. 4400, The)|	Breaking Bad|
|${showNote}<br>${tvShow.note} | TV show note | My personal note |
|${videoResolution}<br>${episode.mediaInfoVideoResolution} |	Video resolution (e.g. 1280x720)|	1280x720|
|${aspectRatio}<br>${movie.mediaInfoAspectRatioAsString}  |Aspect ratio (e.g. 178)| 240|
|${aspectRatio2}<br>${movie.mediaInfoAspectRatio2AsString}  |2nd aspect ratio (e.g. 178, only for multiformat)| 178|
|${videoFormat}<br>${episode.mediaInfoVideoFormat} |	Video format (e.g. 720p)	|720p|
|${videoCodec}<br>${episode.mediaInfoVideoCodec} |	Video codec (e.g. h264)	|h264|
|${videoBitDepth}<br>${episode.mediaInfoVideoBitDepth} |	Video bit depth (e.g. 8 / 10)	|8|
|${videoBitRate}<br>${episode.mediaInfoVideoBitrate;bitrate}  |Video bitrate (e.g. 10.4 Mbps)| 10.4 Mbps |
|${audioCodec}<br>${episode.mediaInfoAudioCodec} |	Audio codec of the first stream (e.g. AC3)	|AAC|
|${audioCodecList}<br>${episode.mediaInfoAudioCodecList} |	Array of all audio codecs (e.g. [AC3, MP3])	|[AAC, MP3]|
|${audioCodecsAsString}<br>${episode.mediaInfoAudioCodecList;array} |	List of all audio codecs (e.g. AC3, MP3)	|AAC, MP3|
|${audioChannels}<br>${episode.mediaInfoAudioChannels} |	Audio channels of the first stream (e.g. 6ch)	|6ch|
|${audioChannelList}<br>${episode.mediaInfoAudioChannelList} |	Array of all audio channels (e.g. [6ch, 2ch])|	[6ch, 2ch]|
|${audioChannelsAsString}<br>${episode.mediaInfoAudioChannelList;array} |	List of all audio channels (e.g. 6ch, 2ch)	|6ch, 2ch|
|${audioLanguage}<br>${episode.mediaInfoAudioLanguage} |	Audio language of the first stream (e.g. EN)|	en|
|${audioLanguageList}<br>${episode.mediaInfoAudioLanguageList} |	Array of all audio languages (e.g. [EN, DE])|	[en, de]|
|${audioLanguagesAsString}<br>${episode.mediaInfoAudioLanguageList;array} |	List of all audio languages (e.g. EN, DE)|	en, de|
|${mediaSource}<br>${episode.mediaSource} |	Media source (DVD etc)|	TV|
|${hdr}<br>${episode.videoHDRFormat} |	High Dynamic Range (HDR)|	HDR|
|${hdrformat}<br>${movie.videoHDRFormat}|	High Dynamic Range (HDR) Format	 | HDR 10|
|${filesize}<br>${episode.videoFilesize;filesize} | File size in GB | 1.05 G |
|${parent}<br>${tvShow.parent} |	Path between datasource and parent folder of the TV show|	B|
|${note}<br>${episode.note} | Episode note | My personal note |

### Renderers ###

You can also use some pre-built renderers to modify the result; you cannot use the preset token for the renderers to work - you need to use the full syntax:
* _UPPER_ case renderer `upper`: `${tvShow.title;upper}`
* _lower_ case renderer `lower`: `${tvShow.title;lower}`
* _Title_ case renderer `title`: `${tvShow.title;title}`
* First character upper case renderer `first`: `${movie.title;first}`
* Date format renderer `date`: `${episode.firstAired;date(yyyy-MM-dd)}` would cause the aired date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.
* File size renderer `filesize`: `${episode;filesize(G)}` to format the video filesize to the unit you want (K, KB, KiB, M, MB, MiB, G, GB, GiB are supported. G is the default if you don't add a unit)
* Replacement renderer `replace`: `${episode.title;replace(umlauts.csv)}` would load replacement tokens from the file `/data/umlauts.csv` and to a search and replace of the movie title for every line in the csv. The csv needs to be comma separated and only the comma itself needs to be quoted with double quotes
* Bitrate renderer `bitrate`: `${episode.mediaInfoVideoBitrate;bitrate(Mbps)}` to format the bitrate to the unit you want (k, kb, kbps, M, Mb, Mbps are supported. Mbps is the default if you don't add a unit)

## Advanced Options ##

* **Replace spaces in the TV show folder name with** / **Replace spaces in the season folder name with** / **Replace spaces in the filename with**: You can replace all spaces with either underscores, dots or dashes by activation this option.
* **Replace colons with**: Since colons are illegal characters in folder names and filenames, tinyMediaManager offers you to choose how they should be replaced.
* **Replace non ASCII characters with their basic ASCII equivalents**: Some file systems might need to have ASCII conform file names - by activating this option tinyMediaManager tries to convert non ASCII characters into a similar ASCII character (e.g. Ä - Ae; ß - ss, ...).
* **First character number replacement**: If you use the renderer `;first` (like in `${title;first}`) and the first character would be a digit, replace the digit with the given character.