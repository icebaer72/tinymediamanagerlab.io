---
layout: single
permalink: "/docs/tvshows/filters"
title: "Filter Options"

toc: false

sidebar:
  nav: "docs"

gallery:
  - url: images/docs/tvshows/tvshows21.png
    image_path: images/docs/tvshows/tvshows21.png
  - url: images/docs/tvshows/tvshows22.png
    image_path: images/docs/tvshows/tvshows22.png

---

There are many filters available in the TV shows section which help you to find/filter the TV shows/episodes you are searching for. These filters are applied either to meta data or to the media information of your TV shows/episodes.

{% include gallery id="gallery" %}

You can filter the TV show list by the following items:

* New episodes
* Show duplicates: a duplicate search of episodes on the season/episode combination will be performed
* Watched flag: you can either display all watched or all unwatched episodes
* Genre: show all TV shows/episodes which have at least the selected genre
* Studio
* Crew/Cast member: show all TV shows/episodes containing an actor which name matches to the inserted text
* Tag: show all TV shows/episodes having at least the given tag(s)
* Hide empty TV shows
* Resolution
* Video codec
* Aspect ratio
* Frame rate
* Container format
* Audio codec
* Data source
* Missing meta data: show all episodes with missing meta data
* Missing artwork: show all episodes with missing artwork
* Missing subtitles: show all episodes without subtitles
* Show only missing episodes
