---
layout: single
permalink: "/docs/kodi"
title: "Kodi JSON-RPC API"

sidebar:
  nav: "docs"
---
Using the [Kodi JSON-RPC API](https://kodi.wiki/view/JSON-RPC_API) you are able to connect your tinyMediaManager instance to your Kodi instance and remotely control some parts of Kodi. To make this work, you need to set up Kodi to [accept incoming connections](https://kodi.wiki/view/Settings/Services/Control) by enabling _Allow remote control via HTTP_:

<a class="fancybox" href="{{ site.urlimg }}2021/05/kodi-http.jpg" rel="post" title="Allow remote control via HTTP">![Allow remote control via HTTP]({{ site.urlimg }}2021/05/kodi-http.jpg "Allow remote control via HTTP"){: .align-center}</a>

After you have enabled the remote control in Kodi you can set up tinyMediaManager to connect to the Kodi instance. Inside tinyMediaManager go to "Settings -> General -> External devices" and enter the corresponding values of your Kodi setup. Afterwards you can test the connection to the Kodi instance.

At the moment the following functions are supported by tinyMediaManager:

- Remote control of Kodi
  - Mute
  - Set Volume
  - Quit
  - Hibernate/Reboot/Shutdown/Suspend System
- Manage data sources (Video/Audio)
  - Clean (remove non existent entries)
  - Scan (search for new items)
- Refresh movies/episodes metadata from NFO
- Receive events from Kodi