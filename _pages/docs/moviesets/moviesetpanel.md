---
layout: single
permalink: "/docs/moviesets/moviesetpanel"
title: "Movie Set Panel"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"

gallery1:
  - url: images/docs/moviesets/moviesets01.png
    image_path: images/docs/moviesets/moviesets01.png
gallery2:
  - url: images/docs/moviesets/moviesets02.png
    image_path: images/docs/moviesets/moviesets02.png    
gallery3:
  - url: images/docs/moviesets/moviesets03.png
    image_path: images/docs/moviesets/moviesets03.png

---

The movie set panel is the main panel in the movie set management section. It consists of two main parts:

* Movie set list (or often called movie set tree - left hand side)
* Movie set information (right hand side - if a movie set is selected).
  * Details - overview of the movie set
  * Artwork - a panel full with the artwork of your movie set
* Movie information panel (right hand side - if a movie set is selected).
  * Details - common movie details like title, release date, certifications, ...
  * Cast and crew
  * Media files - everything about the files of your movie
  * Artwork - a panel full with the artwork of your movie
  * Trailer - a list of all scraped trailers (URLs and trailer files)

{% include gallery id="gallery1" %}

## Movie Set List ##

The movie set list shows all existing movie sets of your movie collection. Each movie can be assigned to exactly one movie set, but a movie set can contain more than one movies.

In the movie set list you have a basic overview of the main information about the movie set as extra columns:

* Movie set title
* Number of assigned movies
* Rating (the main rating which you have chosen in the settings)
* Video format
* Size of the video file(s)
* NFO - is there a NFO for this movie?
* Artwork - is there artwork for this movie set (or movie)?
* Watched - has the movies of this set been marked as watched

## Movie Set Information ##

### Movie Set Details ###

{% include gallery id="gallery1" %}

The movie set details panel shows the main artwork (poster and fanart) and holds all relevant data for the movie set itself:

* Title
* Plot
* A list of all associated movies of this movie set

### Artwork ###

{% include gallery id="gallery2" %}

The artwork panel is a collection of all existing artwork for a movie.

## Movie Information ##

{% include gallery id="gallery3" %}

If you select a movie from the tree you will see the same information as in the [movie panel](/docs/movies/moviepanel).
