---
layout: single
permalink: "/docs/moviesets/settings"
title: "Movie Set Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Movie Set Settings #

## Movie Sets ##

### Artwork ###

For movie sets you have basically two locations to store the artwork in:

- The movie folder(s) of every movie in the movie set
- A dedicated artwork folder containing the movie set artwork

Using these two possible locations for movie set artwork, you can store the artwork in three different ways:

- Movie folder style: `<movie folder>/movieset-<artwork type>.ext`
- Kodi/Artwrok Beef style: `<movie set artwork folder>/<movie set name>/<artwork type>.ext`
- Movie Set Artwork Automator style: `<movie set artwork folder>/<movie set name>-<artwork type>.ext`

`.ext` stands for the image format of the artwork file which should be either `.png`, `.jpg` or `.gif`.

The following artwork types and filenames are available:

* **Poster**
  * \<movie folder\>/movieset-poster.ext
  * \<movie set artwork folder\>/\<movie set name\>/poster.ext
  * \<movie set artwork folder\>/\<movie set name\>-poster.ext
* **Fanart**
  * \<movie folder\>/movieset-fanart.ext
  * \<movie set artwork folder\>/\<movie set name\>/fanart.ext
  * \<movie set artwork folder\>/\<movie set name\>-fanart.ext
* **Banner**
  * \<movie folder\>/movieset-banner.ext  
  * \<movie set artwork folder\>/\<movie set name\>/banner.ext
  * \<movie set artwork folder\>/\<movie set name\>-banner.ext
* **Clearart**
  * \<movie folder\>/movieset-clearart.ext  
  * \<movie set artwork folder\>/\<movie set name\>/clearart.ext
  * \<movie set artwork folder\>/\<movie set name\>-clearart.ext
* **Thumb**
  * \<movie folder\>/movieset-thumb.ext  
  * \<movie folder\>/movieset-landscape.ext  
  * \<movie set artwork folder\>/\<movie set name\>/thumb.ext
  * \<movie set artwork folder\>/\<movie set name\>/landscape.ext
  * \<movie set artwork folder\>/\<movie set name\>-thumb.ext
  * \<movie set artwork folder\>/\<movie set name\>-landscape.ext
* **Logo**
  * \<movie folder\>/movieset-logo.ext  
  * \<movie set artwork folder\>/\<movie set name\>/logo.ext
  * \<movie set artwork folder\>/\<movie set name\>-logo.ext
* **Clearlogo**
  * \<movie folder\>/movieset-clearlogo.ext    
  * \<movie set artwork folder\>/\<movie set name\>/clearlogo.ext
  * \<movie set artwork folder\>/\<movie set name\>-clearlogo.ext
* **Disc art**
  * \<movie folder\>/movieset-disc.ext    
  * \<movie folder\>/movieset-discart.ext
  * \<movie set artwork folder\>/\<movie set name\>/disc.ext
  * \<movie set artwork folder\>/\<movie set name\>/discart.ext
  * \<movie set artwork folder\>/\<movie set name\>-disc.ext
  * \<movie set artwork folder\>/\<movie set name\>-discart.ext
